package main

import (
	"fmt"
)

func main() {
	f := foo()
	f()
	fmt.Println(f())
	fmt.Println("Hello, playground")
}

func foo() func() int {
	return func() int {
		return 42
	}
}
