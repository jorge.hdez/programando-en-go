package main

//\n
// >
//<
import (
	"fmt"
)

func main() {
	x := "Hola Mundo"
	y := `esta es una linea
	partida`

	fmt.Println(x)
	fmt.Printf("el tipo de dato de x es : %T\n", x)
	fmt.Println(y)
	fmt.Printf("El tipo de dato de y es : %T\n", y)
	fmt.Println("")
	bs := []byte(x)
	fmt.Println(bs)
	fmt.Printf("%T", bs)

	fmt.Println("")
	for i := 0; i < len(x); i++ {
		fmt.Printf("%#U", x[i])
	}
	fmt.Println("-----------------------------------------")

	for i, v := range x { 
		fmt.Printf("En el indice %d el valor es %#x \n", i, v)
	}
	fmt.Println("")
	fmt.Printf("con el verbo %q indico que  se imprima el string %s", "%s", x)
}
