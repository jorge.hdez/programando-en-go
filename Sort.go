package main

import (
	"fmt"
	"sort"
)

func main() {
	xi := []int{1, 5, 3, 6, 8, 4, 7, 10, 9}
	xs := []string{"James", "Q", "J", "Deduardo"}

	fmt.Println(xi)
	sort.Ints(xi)
	fmt.Println(xi)
	sort.Strings(xs)
	fmt.Println(xs)
}
