package main

import (
	"encoding/json"
	"fmt"
)

type persona struct {
	Nombre   string
	Apellido string
	Edad     int
}

func main() {
	p1 := persona{
		Nombre:   "Jorge",
		Apellido: "Hernandez",
		Edad:     12,
	}
	p2 := persona{
		Nombre:   "Simon",
		Apellido: "Hernandez",
		Edad:     100,
	}

	personas := []persona{p1, p2}
	fmt.Println(personas)

	bs, err := json.Marshal(personas)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(bs))
}
