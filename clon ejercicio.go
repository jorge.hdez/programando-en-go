package main

import "fmt"

type persona struct {
	nombre      string
	apellido    string
	cacionesFav []string
}

func main() {
	p1 := persona{
		nombre:      "jorge",
		apellido:    "hernandez",
		cacionesFav: []string{"rock", "metalika"},
	}
	p2 := persona{
		nombre:      "marcos",
		apellido:    "hernandez",
		cacionesFav: []string{"maluma", "huapango"},
	}
	fmt.Println(p1.nombre)
	fmt.Println(p2.apellido)
	for i, v := range p1.cacionesFav {
		fmt.Println("\t", i, v)
	}
	fmt.Println(p2.nombre)
	fmt.Println(p2.apellido)
	for i, v := range p2.cacionesFav {
		fmt.Println("\t", i, v)
	}

}
