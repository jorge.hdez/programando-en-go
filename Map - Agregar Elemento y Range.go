package main

import "fmt"

func main() {

	m := map[string]int{
		"Batman": 32,
		"Robin":  27,
	}
	fmt.Println(m)
	fmt.Println(m["Batman"])

	v, ok := m["Jorge"]
	fmt.Println(v)
	fmt.Println(ok)

	if v, ok := m["Batman"]; ok {
		fmt.Println("Imprimiendo desde el if", v)
	}
	m["Jorge"] = 31
	for llave, valor := range m {
		fmt.Println(llave, valor)
	}
}
