package main

import (
	"fmt"
)

func main() {
	//xi := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	//x := sum(2, 3, 4, 5, 6, 7, 8, 9)
	x := sum()
	fmt.Println("el total almacenado es", x)

	

}
func sum(x ...int) int {
	fmt.Println(x)
	fmt.Printf("%T\n", x)

	suma := 0
	for i, v := range x {
		suma += v
		fmt.Println("El valor en el índice", i, "suma", v, "al total, quedando", suma)
	}
	fmt.Println("El total es", suma)
	return suma
}
