package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Printf("Numero de CPUs: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Gorutinas: %v\n", runtime.NumGoroutine())
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		fmt.Println("Hola desde la primera Gorutina")
		wg.Done()

	}()

	go func() {
		fmt.Println("Hola desde la segunda Gorutina")
		wg.Done()
	}()
	fmt.Printf("Numero de CPUs: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Gorutinas: %v\n", runtime.NumGoroutine())
	wg.Wait()
	fmt.Println("A punto de Finalizar")
	fmt.Printf("Numero de CPUs: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Gorutinas: %v\n", runtime.NumGoroutine())

}
