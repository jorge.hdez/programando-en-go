package main

import (
	"fmt"
)

type persona struct {
	nombre string
}

func main() {
	p1 := persona{
		nombre: "jorge",
	}
	fmt.Println(p1)
	cambiame(&p1)
	fmt.Println(p1)
	fmt.Println("Hello, playground")
}
func cambiame(p *persona) {
	p.nombre = "sefe"
	//(*p).nombre = "hs"
}
