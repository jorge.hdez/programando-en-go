package main

import (
	"fmt"
)

func main() {
	a := 42
	b := 42
	fmt.Printf("%d\t%b\t%x\n", a, a, a)
	fmt.Printf("%d\t%b\t%x\n", b, b, b)
}
