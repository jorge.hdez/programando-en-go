package main

import (
	"fmt"
)
//
//los canales bloquean
func main() {
	ca := make(chan int)
	//canal sin buffer (unbuffered chanel)
	go func() {
		ca <- 42
	}()
	fmt.Println("Chanels")

	fmt.Println(<-ca)
}
