package main

import (
	"fmt"
)

func main() {
	switch {
	case 2 == 4, 8 == 8:
		fmt.Println("no deberia de imprimir, ahora si")
	case 3 == 3:
		fmt.Println("imprimir en pantalla")
		fallthrough
	case 4 == 5:
		fmt.Println("no deberia de imprimir")
	default:
		fmt.Println("imprimiendo desde deafult")
	}
	//----------------ejemplo con datos
	switch "manzana" {
	case "fresa", "limon":
		fmt.Println("frutas verdes")
	case "manzana":
		fmt.Println("futas rojas")
	default:
		fmt.Println("sin frutas")
	}
}
