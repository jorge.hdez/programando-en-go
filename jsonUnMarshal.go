package main

import (
	"encoding/json"
	"fmt"
)

type persona struct {
	Nombre   string `json:"Nombre"`
	Apellido string `json:"Apellido"`
	Edad     int    `json:"Edad"`
}

func main() {
	fmt.Println("Hello, playground")

	s := `[{"Nombre":"Jorge","Apellido":"Hernandez","Edad":12},{"Nombre":"Simon","Apellido":"Hernandez","Edad":100}]`
	bs := []byte(s)
	fmt.Printf("%T\n", s)
	fmt.Printf("%T\n", bs)

	var personas []persona

	err := json.Unmarshal(bs, &personas)

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("toda la data", personas)

	for i, v := range personas {
		fmt.Println("\nPERSONA NUMERO", i+)
		fmt.Println(v.Nombre, v.Apellido, v.Edad)
	}
}
