package main

import (
	"fmt"
)

type persona struct {
	nombre   string
	apellido string
	edad     int
}

func (p persona) presentar() {
	fmt.Println("HOla, soy", p.nombre, p.apellido, "y tengo", p.edad, " Años")
}

func main() {
	p1 := persona{
		nombre:   "Jorge",
		apellido: "Hernandez",
		edad:     22,
	}
	p1.presentar()
	fmt.Println("Hello, playground")
}
