package main

import (
	"fmt"
)

func main() {
	s1 := foo()
	fmt.Println(s1)
	fmt.Printf("%T\n", bar()())

	fmt.Println(bar())
}

func foo() string {
	return "hola mundo"
}

func bar() func() int {
	return func() int {
		return 1492
	}

}
