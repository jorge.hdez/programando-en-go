package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	fmt.Println("Numero de CPUs", runtime.NumCPU())
	fmt.Println("Numero de Gorutinas", runtime.NumGoroutine())

	contador := 0

	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)

	for i := 0; i < gs; i++ {
		go func() {
			v := contador
			v++
			runtime.Gosched()
			contador = v
			wg.Done()
		}()
		fmt.Println("Numero de Gorutinas", runtime.NumGoroutine())
	}
	wg.Wait()
	fmt.Println("cuenta: ", contador)
	fmt.Println("Hello, playground")
}
