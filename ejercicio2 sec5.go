package main

import (
	"fmt"
)

func main() {
	a := (42 == 42)
	b := (42 <= 42)
	c := (42 >= 41)
	d := (42 != 42)
	e := (41 > 42)
	f := (41 < 42)
	fmt.Println(a, b, c, d, e, f)

}
