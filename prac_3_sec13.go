package main

import (
	"fmt"
)

func main() {
	defer foo()
	fmt.Println("Hello, playground")
}

func foo() {
	defer func() {
		fmt.Println("defer")
	}()
	fmt.Println("foo corrio")
}
