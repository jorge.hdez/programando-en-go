package main

import (
	"fmt"
)

func main() {
	foo()
	func() {
		fmt.Println("la funcion anonima se ejecuto")
	}()

	func(x int) {
		fmt.Println("El significado de la vida es: ", x)
	}(42)
}

func foo() {
	fmt.Println("el foo se ejecuto")
}
