package main

import (
	"fmt"
)

func main() {
	ca := make(chan int, 2)
	//canal con buffer (unbuffered chanel)
	ca <- 42
	ca <- 43

	//bufferexitoso.go

	fmt.Println("Chanels")

	fmt.Println(<-ca)
	fmt.Println(<-ca)
}
