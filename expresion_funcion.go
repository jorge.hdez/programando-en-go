package main

import (
	"fmt"
)

func main() {
	f := func() {
		fmt.Println("mi primera expresion funcion")
	}
	f()

	g := func(x int) {
		fmt.Println("yo naci el dia: ", x)
	}
	g(1492)
}
